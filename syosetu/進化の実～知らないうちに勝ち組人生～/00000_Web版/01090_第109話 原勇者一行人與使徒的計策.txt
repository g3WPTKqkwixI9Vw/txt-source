﻿
在尤斯多他們壓制著魔神教團使徒的時候，魔族軍歸入『劍騎士』露易絲露率領的【劍聖之戰乙女】，S級冒險者們歸入『黑之聖騎士』率領的【奈落黑兵團】兩方面，各自朝著城牆外出擊。

「喂，妮姆！你也差不多該起來了！你不在的話就不能補強『結界』啊！」
「嗯～......？」

被瓦爾搖晃，妮姆擦著眼睛醒來。

「呼啊～......什麼～......？我還很睏啊～......。」
「這場戰鬥結束的話你想睡多久都行。總之，快點將原本設有的『結界』補強。」
「嗯～......雖然不太懂～......好～的......。」

妮姆依舊很睏倦地，拿起佩在腰間的法杖舉向天空。

「呼啊～......可這原本我是為了阿爾醬而設下的『結界』啊～......啊，說起來......回來這之後就沒見過阿爾醬啊～......她究竟怎樣了呢～？離開這個『結界』的話可是很危險的呀～......她還在這裡嗎～？」
「喂，這些事你稍後再想！」
「是是，我知道啦～......『結界術：除魔之層』～」

妮姆以被瓦爾拖著的狀態詠唱魔法。
瞬間，朝著天空舉著的法杖發出青白色的光球飛行天空。
那個光球在上空散開，將整個王都提魯貝爾覆蓋。
────妮姆．多盧米璐。
她是以『睡公主』這一稱號被廣為人知的S級冒險者，在這個提魯貝爾又在別的意義上很有名。
那就是將這個提魯貝爾包圍的『結界』。
其實她跟阿爾托莉雅是友人，為了阿爾托莉雅的不行體質調查了不少事情。
還有為了抑制住阿爾托莉雅的不幸體質而在提魯貝爾展開了結界，所以提魯貝爾至今才沒有因為阿爾托莉雅的不幸體質遭到損害。

「好，幹得好！那麼就這樣直接────。」
「Zzz。」
「你睡得也太快了吧！還有魔物在這裡所以你別睡了！」
「姆！在那裡的可愛的小姐！？小姐！請與我一起度過熱情的一夜────。」
「喂由莉涅！？你這傢伙打算去哪裡！？噠啊啊啊啊啊啊啊！尤斯多，你還真的不是一般的辛苦啊！？」

光是控制過於自由的S級冒險者們，瓦爾就已經精疲力盡了。
即便如此也總算是來到了『黑之聖騎士』那邊，在那邊以『雷女帝』艾蕾米娜為中心，展開著與魔物的激烈戰鬥。

「喂喂......這麼看來還真是絕望啊......。」

瓦爾不禁對看到的情況發出感想時，艾蕾米娜正與眼前的龍對峙。

「看來真的只有S級魔物呢......【劍龍】什麼的還真是很久沒見了？」
「噶哦哦哦哦哦哦哦哦哦哦哦哦！」

與艾蕾米娜對峙的魔物全身長著尖銳的刀刃，那就是異形的龍【劍龍】。
那每一把刀刃都有不遜於一流鐵匠打出來的劍的鋒利程度，由於龍系統的魔力之高，每一把刀刃都能纏繞魔法進行攻擊。
但是────。

「很遺憾，你跟我的相性是最差哦？」

艾蕾米娜這麼說，將右手伸向劍龍。

「『雷極槍』」

從艾蕾米娜的手臂出現大量的雷槍接著合而為一，變成極大的一根雷槍。
那比校內對抗戰時羅貝魯多對阿格諾斯使出全力生成的雷槍還要大。
生成的雷槍以劍龍無法認知的速度飛翔，輕而易舉地貫穿了它長滿刀刃的身體。

「咕噶啊啊啊啊啊啊啊啊啊啊啊！」

被貫穿的劍龍全身都遭到雷擊，觸電冒出黑煙倒下了。
看到這情況瓦爾傻眼地說。

「強到這程度的王妃找遍全世界也找不到第二個吧。」
「是呢......那麼，也不能光讓艾蕾米娜大人戰鬥。我也要上了。」

接著可妮莉婭用扇子遮住嘴角以優雅的步伐走前。

「原本的話，你們可以不被打倒安靜地度過生活的......。」

以有點憐憫的表情看著眼前的魔物大群的可妮莉婭，用纖細的手指指著它們。

「至少，讓你們回歸大地的懷抱吧。」

這麼說完的瞬間，周圍的大地如同包圍魔物大群一般開始腐蝕。

「腐朽，沉沒吧。『腐海』」

腐蝕的大地最終來到魔物群的腳下，腐敗的地面化為無敵的沼澤。

「噶啊啊啊啊......噶，噶噶────。」
「嘎咿咿咿咿咿咿咿！嘎咿，嘎嘎────。」
「咕噢噢噢噢噢噢噢哦哦，哦哦，哦────。」

魔物們拚命地亂竄打算脫身，但越是行動越是沉得快，最終完全下沉沉默了。
剛才還是沼澤的大地變回原狀，將一切放入眼中的可妮莉婭閉上扇子。

「來世，願你們能夠安穩度過。」

可妮莉婭．阿爾娜露緹。
她是世界上唯一一位操縱『腐屬性魔法』的存在，由於這魔法和身為貴族而有著【貴腐人】的稱號。

「哈啊......赫蒙大人看來不在這裡呢......不知道在不在那邊呢？」

......絕對，不是因為是那條道上的人所以才稱呼她為【貴腐人】......希望是這樣。
在可妮莉婭清掃魔物群的時候，同樣是S級冒險者的由莉涅以驚人的速度砍倒魔物。

「真是的......都是因為你們害我不能滿足地搭訕不是嗎......！」

雖然與魔物戰鬥的動機不純，但她的實力是的貨真價實的，現在也確實地減少著魔物的數量。

「你們以為光是這條街，就有多少女性啊！？居然敢襲擊這裡......即便神原諒了我也絕不原諒！」

由莉涅當場大跳躍，將劍朝著上空舉起。

「狂吹肆虐吧────【百合斬】！」

接著，以由莉涅拿著的劍為中心不知從哪裡彙集著花瓣。
然後，纏繞著花瓣的劍以猛烈的氣勢揮下後，化為不認為是花瓣的鋒利劍刃，朝著周圍的魔物飛舞。

『嘎啊啊啊啊啊啊啊啊啊！』
「吵死了！給我閉上嘴去死！」
『嘎啊，嘎啊！？嘎────』

不講道理的理由連臨死前的吶喊都不被允許，魔物消失了。
由莉涅．蕾絲。
如同百合一般惹人憐愛的討伐身姿，因此被稱為『百合劍士』的她。
......如今卻變為了別的意義的『百合騎士』，只能讓人說聲遺憾。

「嗯─......有不少沒見過的魔物混在裡面啊......。」

饒著超巨大爆炸頭的阿福羅斯這麼說。
朝著這位阿福羅斯，一隻魔物衝了過來。

「嗯？那傢伙是......。」

以阿福羅斯喂目標突擊的是在S級魔物之中也擁有頂級防禦力的【守衛龍】。
他的鱗片一般的武器連刮痕都做不到，是與在場的S級冒險者中的由莉涅和伽魯甘多的相性最差的魔物。

「嘎哦哦哦哦哦哦！」
「呼姆......這傢伙。的確是硬的亂七八糟～......。」

阿福羅斯如同在說真麻煩一般地嘆氣後，守衛龍活用著它的防禦力進行突進。
就這麼相撞的話，即便是S級冒險者也不可能平安無事，面對這樣危險的突進阿福羅斯沒有一絲焦急。

「嘛，到最後還是跟平時一樣就是了。」

在阿福羅斯和守衛龍將要相撞的時候，阿福羅斯突然低頭。
其結果，阿福羅斯的超巨大爆炸頭必然會與守衛龍產生衝突────。

「嗯，進去了呢。」

沒想到，守衛龍居然就這麼被爆炸頭吞沒了。
接著如同什麼事都沒發生於一般抬起頭，阿福羅斯朝著自己的爆炸頭伸手進去，抓住某物直接拔出。

「喲哆......果然挺大個的。」

拿出來的，是剛才與之對峙的守衛龍的骨頭。

「戰鬥雖然麻煩，不過能收集到素材還是很值得高興的。」

這麼說著，阿福羅斯以像要用鼻子哼歌一般的氛圍接連將魔物收納，只取出骨頭。
阿福羅斯．提諾瓦爾。
他是開拓被稱為未開發的土地或是魔境這樣的地方的冒險者，由於他身上的謎團過多而被賦予了『魔境』這一稱號的罕見的冒險者。

「伽魯甘多不上嗎？」
「啊？因為沒有人形的魔物啊。我就算了。」
「你......還真是一如既往地我行我道啊......話雖如此，看這情況也不用我們出場了。」

伽魯甘多與瓦爾眺望著眼前的光景，這麼聊道。
事實上，艾蕾米娜、可妮莉婭、由莉涅、阿福羅斯四人再加上『黑之聖騎士』率領的【奈落黑兵團】，魔物的全滅也只是時間的問題。
如此這般，S級冒險者雖然主要是由變態們構成的，但他們的實力毫無虛假。

◆◇◆

「這個......。」
「......光看數量的話的確是絕望的......。」

魔族軍成員來到由『劍騎士』露易絲率領的【劍聖之戰乙女】這邊。
他們來到城牆外，對襲來的魔物數量表示絕句。
不過，多虧了露易絲和【劍聖之戰乙女】的努力，她們並沒有允許魔物接近城牆。
眺望著這樣的光景，蕾婭向澤洛斯發問。

「那麼......怎樣？它們會聽我們說嗎？」
「......不行呢。全都不是我們魔族軍所管轄的魔物。而且，看來還失去了理性......。」

魔族軍有不少魔物，而且還支配著它們。
為此，根據魔物的種類下達強制命令就能停止它們的行動，但看到襲來的魔物的種類，澤洛斯判斷是不可能的。

「真麻煩啊。不這麼做這樣全部殺了不就好了嗎？」

索璐亞打著呵欠這麼說後，索璐亞的身體被『黑暗』覆蓋。
纏繞著黑暗的索璐亞，以真紅的眼瞳看向魔物群。

「刺穿吧。」

接著，纏繞著索璐亞身體的黑暗的一部分化為尖銳的長槍，一起襲向魔物群。
暗槍途中分支增加數量，不斷殲滅著魔物群。

「噢啦，你在發什麼呆啊？你也給我戰鬥啊。」
「......雖然順從你的話並非我本意，不過也是呢。」

這麼說的瞬間，澤洛斯噴出災禍的魔力。

「畢竟是這種數量。我的魔力有界限，但你這傢伙沒問題吧？」
「哈啊！跟你不同，我可不用消耗魔力。不過，光我一人無法對付這個數量。你就儘量消耗你的魔力好了。」
「這不用你說。」

澤洛斯將雙手伸前，災禍的魔力經過雙臂最終收束到雙掌。

「『消滅炮』」

一口氣解放收束到雙掌的魔力，蘊含驚人力量的奔流將魔物群包圍。
被澤洛斯的魔法打中的魔物，連存在都無法維持，完全消滅了。

「雖然你要怎麼做是你的事，不過可別衝太前早早就掛掉啊？」
「你這是在擔心誰啊？你才是，別一時大意死掉啊？」

這麼聊著，兩人不斷減少魔物的數量。

「......這兩人還是一如既往地怪物啊......。」
「是呢......那麼，該怎麼辦？吾輩也出擊嗎？」
「如果我們也出擊的話，防守就太薄弱了吧？應該無法完全保護如此巨大的城牆吧......。」

留下來的蕾婭他們，眺望著澤洛斯他們的戰鬥商量道。

「是呢......我們就在防禦薄弱的地方努力吧。」

蕾婭如此決斷，就在她打算開始移動的時候。

「────還挺能幹的嘛。」
「！是誰！？」

突然傳來的聲音讓蕾婭他們環視四周。
而後，一位男性不知何時站在了城牆上。

「你是......。」
「......也是。姑且報一下名號吧。我是羅迪亞斯。【魔神教團】的使徒。」
「......使徒，呢......。」

自稱羅迪亞斯的男人，是個讓人難以捕捉氣息的存在。
留著能遮住右眼的紫髮，從中窺視的尖銳視線盯著蕾婭他們。

「使徒大人找我們有何要事呢？」
「......你們應該從雷思達那傢伙那裡聽說了吧。我們的目的只有一個。那就是魔神大人的復活。」
「是嗎......而你在這裡也就是說......。」

蕾婭惡視線變得尖銳的同時，在她背後的麗雅蕾塔和烏魯斯的視線也尖銳起來。

「......跟你們想的一樣，是來排除你們的。」
「那麼......在被幹掉之前幹掉你......！」

蕾婭纏繞著火焰，朝著羅迪亞斯突擊。

「蕾婭，我來支援你！『旋風噴射』！」

蕾婭通過麗雅蕾塔詠唱的魔法獲得加速。

「......呼姆，真少見啊。不死鳥的魔族嗎。」
「沒錯哦。所以，就讓我用這火焰將你燒盡......！」
「哼......這可難說？」

羅迪亞斯輕易就避開了蕾婭的加速攻擊。

「什！？？」
「......我在使徒之中也是以戰鬥為主的實戰部隊中的一人。很遺憾，這種程度的攻擊連擦傷都做不到哦？」
「那麼，這樣如何？」

接著，從躲開了蕾婭攻擊的羅迪亞斯背後，烏魯斯全身纏繞鬥氣衝出來。

「『王鬼的一擊』！」

將全身的鬥氣集中在右臂，烏魯斯揍向羅迪亞斯。

「......這邊是王鬼族嗎。有趣，就讓我們比比力氣吧。」
「什麼！？」

然而，羅迪亞斯沒有避開烏魯斯的攻擊，而是正面揍回去。
原本的話，幾乎沒有存在可以接住擁有怪力的鬼族......而且還是王鬼族的烏魯斯的攻擊。
但是，羅迪亞斯用自己的拳頭揍烏魯斯的拳頭，將烏魯斯的攻擊完全抵消了。

「吾輩的攻擊......居然如此簡單就......！」
「烏魯斯，退下！『魅惑視線』！」
「......接著是夢魔嗎，魔族軍還真是集合了有趣的成員啊。」

麗雅蕾塔使用夢魔族的特徵魅惑異性的法術，但對羅迪亞斯沒有效果。

「怎麼會......。」
「......怎麼了？戰鬥還只是剛開始而已啊？」
「────那麼，這招如何？」
「！」

突然地，從羅迪亞斯頭上傳來聲音。
那瞬間羅迪亞斯立刻跳開，『劍騎士』露易絲的劍砍在了羅迪亞斯剛才站著的地方。

「......妳是......。」
「我嗎？我是露易絲．帕爾傑。大家稱呼我為『劍騎士』，不過你應該早就知道了吧？」
「......原來如此，維恩布魯古王國的二大騎士之一嗎。看來另一位在另一邊奮鬥著呢......。」
「是的。『黑之聖騎士』的話應該沒問題吧。而且，S級冒險者們也在那邊。」

蕾婭他們雖然對露易絲的登場感到驚訝，不過馬上重整姿勢與歐迪亞斯拉開距離。

「就我所見，看來你有著奇怪的力量啊......。」
「......沒想到居然連這點都看穿了。那麼，該怎麼辦？這次輪到你當我的對手嗎？」

口吻雖然沒變但羅迪亞斯的氛圍已經大不如前，全身冒出刺人的魔力。

「......非常抱歉，那裡的三人。能請你們幫忙嗎？」
「哎？」
「......看來，我一人對付那個人有些負擔過重。如果可以的話希望你們能幫忙......。」

這麼說的露易絲視線變得嚴肅，毫不鬆懈地看著羅迪亞斯。

「......啊啊。我明白了。」
「感謝你們的協助。」

蕾婭接受露易絲的提案，麗雅蕾塔和烏魯斯也各自開始警戒羅迪亞斯。

「......以四人為對手嗎。不過......沒關係。一起上吧。」

以羅迪亞斯的話為信號，四人一齊開始攻擊。

◆◇◆

「那麼，請你老實交代吧？」
「......你們究竟有何目的？」

在被拘束的狀態下的雷思達和邋遢男的面前，拉傑和露提亞質問。
雷思達由席德，而邋遢男由尤斯多壓制著，完全動不了。
然而，即便是處於這種狀態下，邋遢男還是笑著。

「庫庫庫庫庫庫庫......你們，以為壓制著我就算贏了嗎？」
「什麼？」
「死不服輸就算了吧？現在的你能做的了什麼。」

尤斯多稍微加強了按在他脖子上的劍的力道這麼說。
不過，那男人的態度依舊沒變。

「可能吧。不過，【魔神教團】的使徒可不止我們哦。只要有魔神大人的一天，那就會源源不斷地出現。而且，現在羅迪亞斯那傢伙已經收拾掉你們的同伴了吧？」
「你別說傻話。雖然我不知道你說的羅迪亞斯是誰，但派出了那種戰力怎麼可能會輸。」
「誰知道呢......雖然你怎麼想是你的自由？不過，羅迪亞斯可是跟我們不一樣是實戰部隊的傢伙。就算被殺了也不要有怨言哦？」

看到依舊保持笑容的男人的情況，拉傑他們皺起臉。

「......走題了呢，快點將你們的目的說出來吧？當然，我們不是在問你復活魔神什麼的。是在問你為什麼要襲擊這裡。」
「呸！誰要告訴你們這────。」
「啊啊，我們盯上了那邊的魔王之女，或者是你的性命。」
「艾德瑪多！？」

雷思達想要隱瞞的事，同樣被壓制著的被稱為是艾德瑪多的男人輕易就說出來，讓雷思達發出驚訝的聲音。

「喂，還真是輕易就說出來了呀。」
「這種事，即便我不說你們也馬上知道吧？」
「......那麼，為什麼盯上我們？」

拉傑這麼問的瞬間，從壓制著他們的席德和尤斯多身上發出了強烈的威壓感。
然而，即便受到這種的威壓感艾德瑪多也毫不介意地笑了。

「庫庫庫庫庫庫庫......為什麼盯上......呢......。」
「嗯？」

拉傑雖然察覺到艾德瑪多的反應很奇怪，但那時艾德瑪多已經行動了。

「很遺憾，現在還盯著你們哦！？」

艾德瑪多開口後，從他的口內出現了一把短劍。
還以為那把短劍朝著拉傑或是露提亞自身飛去，不知為何卻刺在了露提亞的影子上。
突然出現短劍一事，讓尤斯多將艾德瑪多的腦袋使勁砸向地面。

「......你，真的知道你處於什麼立場嗎？」
「露提亞大人！你沒事吧！？」

在尤斯多加強拘束之時，席德壓制著雷思達這麼尋問的時候。

「......滋......────。」
『！？』

露提亞當場倒下了。

◆◇◆

「......真無趣。」
「庫......！」

羅迪亞斯俯視在眼前倒下的露易絲和蕾婭他們這麼說。
在羅迪亞斯的胸口前，有著以前與誠一戰鬥過的提米歐羅斯一樣，畫著邪惡惡魔的紋章。

「......本以為能跟那位『劍騎士』戰鬥，多少有點期待的......真是讓我失望啊。」

看到倒在地面上的蕾婭他們，與魔物為對手的澤洛斯，還有【劍聖之戰乙女】的成員發出悲鳴。

「蕾婭！」
「喂喂......為什麼被打倒了呀！」
「露易絲大人！？」
「庫！你這傢伙！」

以魔物為對手的成員打算馬上前來幫助露易絲他們，但即便被打倒了也不斷湧現的魔物阻礙了他們的進路，使他們無法前來幫忙。
無表情地看著這情況的羅迪亞斯，再次將視線移回露易絲他們身上。

「......很遺憾，你們的救援貌似不回來了。」
「......看來是呢。不過，我也不會輕易被幹掉......！」
「......嚯哦？」

露易絲絞盡力氣站起來，使出神速的一劍。

「......的確很快，吃了這一招會受大傷吧......如果對手不是我的話呢。」
「什！？」

羅迪亞斯這麼說，在原地動也不動以單手拿著的一把小刀就擋住了露易絲的攻擊。

「......那麼，差不多該去看看雷思達他們的情況了。就讓我結束這裡的戰鬥吧。」

這麼說的瞬間，在露易絲和蕾婭他們的周圍瞬間就浮現著幾把小刀。

「......讓我一口氣殺了你們吧。」
「露易絲大人啊啊啊啊啊！」

【劍聖之戰乙女】中的誰如此大喊之時。

「────呼姆，我想這裡應該就是王都提魯貝爾了......。」
「嗚！？......是誰？」

羅迪亞斯馬上對突然傳來的聲音做出反應，接著魔物們被迷之漆黑之槍貫穿，消失飛散了。

「這是......索璐亞，你嗎？」
「......不，這個『黑暗』不是我的。」
「那麼究竟是誰......。」

與索璐亞產生的『黑暗』十分相似的東西，讓澤洛斯不禁這麼問索璐亞，但馬上就被否定了。
在場所有人都冒出疑問號的時候，這次傳來了別的聲音。

「────真是的，按照誠一君的介紹前來，沒想到居然看到同胞被襲擊了......。」
「......你們是......？」

釋放出壓倒性存在感的兩人，以悠然的步伐出現在羅迪亞斯面前。

「────澤阿納斯．傑夫多。只是一介勇者的師傅。」
「────盧修斯．阿爾薩雷。只是一介初代魔王。」
『那不是────』
「做好覺悟了嗎？」
「做好覺悟了嗎？」

────最強的援軍，抵達了。

