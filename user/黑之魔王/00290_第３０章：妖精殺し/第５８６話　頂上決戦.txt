「——『愛之女王』」
喃喃自語著的莉莉，仿彿看著將被處刑的罪大惡極之人般，俯視著仰面躺下的妮露。
妮露倒在競技場上,身上沒有一絲傷痕和污垢，完美如初。仿彿，相信這裡就是自己房間的床一般，好似自己陷入沉眠似的。
「看上去很幸福呢，妮露」
事實上，確實很幸福吧。恐怕，妮露現在沉浸在自己所認為最幸福的生活之中。哪怕只是虛構，但那是將妮露所有的期望實現，甜蜜的，溫柔的.....『夢』
「不知道你是否滿意。這就是第四試練『色慾的世界』哦」
莉莉並沒有小看妮露。習得了古流柔術，裝備了國寶級大魔法具『天空龍掌「蒼天」「紅夜」』的她，如果變成近身戰，甚至能凌駕於沙利葉之上，莉莉給出了如此的評價。如果是正面戰鬥的話，肯定是一場苦戰。最壞的情況，敗北也是有可能的。
正因為如此，莉莉全心全意製作了僅僅只為打倒妮露一人的巨大陷阱——那就是『色慾的世界』
「妮露，你有著與等級五冒險者相符的強大實力，有著和黑乃並肩作戰的實力。但是就算如此，你，還是最弱」
這個陷阱，如果是黑乃和菲奧娜的話看到第一眼的瞬間就能看穿，沒錯，只有打倒了色慾「終焉玫瑰」的人才會注意，從包圍競技場的牆開始，闘技場隨處可見由玫瑰和荊棘組成的裝飾紋樣，都是為了經由視線進行誘導催眠的魔法陣罷了。

當然，不是終焉玫瑰的莉莉，做不到完全覆蓋阿蘇貝魯山脈的洞穴這麼大的範圍，就算是使用天才般的魔法才能來計算，配置的魔法陣，比起真貨的催眠效果也相差很多。
但是，不管怎麼說再現了將『元素掌控者』全員鎖進夢之世界的最強幻術，這就是第四加護『愛之女王』的真正威力。

如果是黑乃，最多只會作為精神防御的手段而使用吧，通過性交而引出各種各樣魔法效果才是原本的使用方式。但是，擁有與生俱來的固有魔法心靈感應的莉莉，能遠超黑乃熟練的使用這個能力。拋開黑乃不談，對能運用幻術的人來多半是理所當然的結果。

萬事具備。但是，能不能成功只有一半機率，就如同賭博一樣。
是否能開頭成功就將三人分開，妮露會中準備好的轉移陷阱嗎？比這些更重要的是，她能從幻術中醒悟過來嗎，或者，就算陷入催眠狀態也能撐到戰鬥結束.
和妮露的單挑。激烈的戰鬥有一半，全部是在現實中發生的。莉莉一邊和妮露戰鬥一邊秘密的持續使用『愛之女王』讓妮露陷入幻術陷阱

雖然使用一種加護的時候，其他的加護無法使用這點非常嚴苛。能打倒沙利葉，全靠『炎之女王』帶來的力量輔助，『雷之女王』的看破，『嵐之女王』的速度，追上了沙利葉才能辦到。也有為了緊急時刻而準備的保險『鋼之女王』

雖然沒有黑乃的加護，和妮露的戰鬥危險十足......但是莉莉贏了。最大限度活用自己的強項，成功的打中了妮露的『弱點』。
「真是不長記性呢，這都已經是第三次了哦。還不懂嗎?你過於強烈的色慾將會毀滅你自己」
妮露的弱點，那就是，對黑乃過於強烈的色慾，莉莉是這麼想的。

偷偷為使用黑乃用過的勺子而暗自開心，害自己被莉莉威脅的事，仍然記憶猶新。這是發生在加拉哈德戰爭開始之前的事，距今還不到一年。
那之後，完美的被菲奧娜的花言巧語欺騙，用當時最不能用的方法接近當時的黑乃，然後被鄭重的拒絕，也是不久之前的事。

妮露被對黑乃的愛情一同生根發芽的性欲耍的團團轉，品嘗了兩次在情敵手中淒慘敗北的滋味。然後，這次她強烈的性欲，又一次成為了決定性的敗因。
「在那個甜美幸福的世界中，絕對無法僅憑自己擺脫出來，你絕對不會醒來」
就連莉莉，也無法擺脫在那個小小的森林小屋和黑乃兩人永遠一起生活的夢境。更別說，終焉玫瑰特化了刺激性方面快樂的淫夢了。這麼美好的世界，妮露怎麼可能放手。
「愚蠢的女人。你那醜陋的感情，使重視你的人的心意都白費了，有怎麼會察覺到呢」
莉莉直到最後都沒一絲失誤。伸出沒有寄宿著任何殺意的手，溫柔的觸摸了妮露的身體。
莉莉是手如同蛇一般順利的鑽進了白衣的領口，經過自己胸部所沒有的巨大山谷，從中摸索，發現了目標物品。那是守護阿瓦隆公主之心，最後的盾牌。
「——『心神守護之白羽』的事情，我知道哦。呼呼，因為黑乃告訴了我」
莉莉在施加幻術的時候，最應該警戒的就是這個有著精神防御效果的大魔法具。其威力，甚至能將有著強大寄生能力來支配數量龐大的魔物組成大軍團的等級五魔物怠惰吉爾的寄生一擊彈開。

如果，這個『心神守護之白羽』對幻術有反應的話，就算是『色慾之世界』被破除的可能性也很高。
但是，有勝算。
「不會解除正面的效果。有利的能力，反而成為敗因了」
說到魔法防御的效果，涉及到多個方面。既有製造僅僅一堵牆的魔法，也有將整個街道覆蓋的巨大結界的魔法。既有僅對惡靈之類發揮效果這種類型的魔法，也有阻斷所有事物這種類型的魔法。

如果嚴密區分『心神守護之白羽』效果的話，既有著阻斷排除來自外部精神干涉的防御能力，也有將來自友方的掩護，也就是說以治癒魔法為代表，對持有者有益處的魔法效果不進行阻斷，擁有這種自主選擇性。

這是相當高度的機能，大部分的精神防御系魔法具都沒有這種功能。正因為將治癒魔法等友方的支援效果遮斷阻礙的類型非常多，所以，冒險者並不需要一定要持有防御系的魔法具。

但是，莉莉正是將其便利性作為了突破口施加了幻術。也就是說，『色慾之世界』被誤認為是一種有治療效果的幻術。與其說是欺騙了『心神守護之白羽』的選擇機能，不如說正因為妮露自身如此期望，所以才會變成接受幻術效果這樣的結果。

實現了打倒莉莉的願望，從哪裡開始，只看到自己所期望的理想未來，所有事情都向自己有利的一方發展，早已深陷其中無法回歸現實。
現在，發展到如願以償的迎來和黑乃結婚初夜的妮露，如今應該在拚命的祈禱著不論發生什麼都不會結束這個甜美的幻夢吧。因為她如此期望著，『心神守護之白羽』才沒有反應。

將取出的『心神守護之白羽』，放入用閃著紅光的魔法陣描繪出的空間魔法中，取而代之，取出了別的魔法具。
那是，沒有任何裝飾的白色環。這正是，可以操作他人最惡劣最差勁的魔法具，『思考支配裝置』。
「讓你見識一下——永遠的噩夢吧」

---

「恭喜您，妮露公主大人！是一個很有活力的孩子哦！」
終於，我產下了黑乃的孩子。值得紀念的第一個孩子誕生了。是女孩。作為母親，暫時完成繁衍後代這一重大職責，我安心了。有著被期望產下繼承人的王族之女的立場，讓我感到了相當大的壓力。

但是，那些不安只是瑣碎的小事罷了。因為，我是如此的深愛著黑乃，並且黑乃也深愛著我。相互愛戀的兩人之間孕育出孩子,就算是命運之神也無法左右，早已決定的絕對真理。

無聊的不安和壓力什麼的，怎樣都好。現在只是，坦率的覺得我和黑乃的愛的結晶平安的來到這個世界，讓人幸福無比。
「妮露，身體還好嗎？很努力了呢，謝謝你」
「嗯，我沒事……太好了，孩子平安的生下來了」
「嗯，是個有活力的女孩哦」
「第一胎生女孩，第二胎生男孩」
「哈哈，太著急了」
但是，看到了黑乃這樣的臉，又變得想要起來了。想要，你的孩子。更想要，和你之間愛的證明。多麼的幸福，心情變得多麼美好，啊，為什麼「愛」是如此美妙的東西呀。
「黑乃，能讓我看看孩子的臉嗎」
「我和妮露的女兒，是個非常漂亮的美人哦」
一邊微笑著，一邊將抱著的女兒讓在床上躺著的我能夠看到。
從純潔的純白絲織布上，窺見如光一般耀眼的女孩的小小的臉龐。
「…誒」
如光一般耀眼？不對，她，真的閃耀著光輝。
「怎麼樣，妮露」
被純白光芒的面紗包裹著，小小的女孩，有閃耀著白金光輝的長髮，翡翠般瞳孔熠熠生輝。
「和妖精永遠可愛吧?」
而且，她的背後像紅靄一樣的東西飄起……不久，化作血一樣鮮紅，充滿了不祥的蝶羽。
「對了，妮露，這個孩子的名字是——」
不對。
不是我的女兒。
這才不是我和黑乃的孩子。
不，怎麼會，為什麼。
因為，那個孩子，不，那個女人——
「就叫莉莉吧」
「不—————————————————————————！！」
（原文「いやぁあああああああああああああああああああああああああああああっ！！」）

---

菲奧娜靜靜的等著那一刻到來。一切都順著計劃進行著。

利用了被莉莉啟動了『歷史的開端』的轉移機能，一口氣侵入莉莉所操縱天空戰艦。這就是作戰的第一階段。
如果能趁奇襲之勢順利的打倒莉莉固然會令人開心，不過不愧是莉莉，這種程度的奇襲並沒有使莉莉動搖。
莉莉有條不紊的發動了反擊，而準備強力魔法的菲奧娜被關進莉莉所發動的結界之中。
「唔，這裡禁止通行哦」
在菲奧娜擺出展開雙手不讓人通過的造型，那是像極了幼女形態莉莉的存在，這正是在結界之中。
由發出紅光的靈體所構成的這個身體，雖然和莉莉非常相似，但也告訴我了說到底只不過是姿態相似的假貨罷了。

也就是說，這就是被稱為『嫉妒雷伊』，黑乃完成最後的試練所要打倒魔物。菲奧娜已經察覺。
「我不會通過哦，看來，單憑我是沒辦法打破這個結界吧」
「是吧，香格里拉的結界可是很厲害的呢！」
到底要模仿菲奧娜所熟知的幼女莉莉的行為到何時，讓人嗤之以鼻，不過是個冒牌貨罷了。
就算非常焦躁，但在眼前的『嫉妒雷伊』並不是本體，只是單純利用光魔法映照出自己身姿的影子罷了。連將其燒成灰也做不到。反之，這傢伙也無法用各種方法攻擊我。

莉莉將菲奧娜關在這裡準備了強力的廣域結界，將其交給『嫉妒雷伊』來發動。

恐怕，『嫉妒雷伊』就是莉莉能保持真正樣子的魔力供給源。有這樣一種說法，擁有強大的力量並寄宿著知性的龍等魔物，人們可以通過契約借與其力量。和這個相似，莉莉通過和『嫉妒雷伊』進行了契約，得到了莫大的魔力，菲奧娜如此在心中解釋。

判明了莉莉力量的秘密，三角帽中的筆記上的秘密只剩少許未曾解開，菲奧娜轉而觀察將自己囚禁的巨大結界。
大小甚至能將這個天空戰艦整個覆蓋隱藏的程度。只有菲奧娜被留在廣大甲板的正中央。

這個結界，並不是莉莉本人的魔法，推測其是天空戰艦所配備的防御機能。不愧是遠超現代的魔法文明所製作的巨大兵器所持有的結界。
就算是菲奧娜，也無法用蠻力打破吧。

發動如此龐大的東西，相比需要花費相當的魔力進行控制吧，正因為如此，才不得不交給『嫉妒雷伊』控制吧。

被分開的看來不僅是我們，也就是說莉莉也削分了自己的戰力。雖然不知道離開『嫉妒雷伊』的莉莉到底能全力戰鬥到何種程度，和原來一樣有著某種程度的時間限制應該是不會錯的。

就這樣被關在這裡，雖然完全順著莉莉的計劃進行著......但是這也在計劃範圍內。並且是往相當好的方面。
菲奧娜不僅沒有怎麼消耗魔力，更沒有負傷，只是變成暫時中斷了和莉莉的戰鬥。就保存戰力來說是非常理想的情況。而且，想知道的情報都已入手了。

首先，在所有加護都會失效的神滅領域，菲奧娜能正常發動黑魔女安迪米昂的加護所賦予的力量『惡魔的存在證明』

莉莉的戰鬥能力來自於妖精本身的固有魔法。正因如此，妖精女王伊利斯的加護也會受到很大影響。如果她想要完全發揮自己的能力，無論如何保證伊利斯的加護能正常發動是必要的。
就算如此莉莉卻將據點放在了『神滅領域阿瓦隆』，只可能是莉莉成功的將這個能夠使加護消失的強大場地效果成功的無效化了。

雖然無法得知到底使用了什麼方法，其中最有可能的應該就是『建立教會』吧。共和國所入侵的地方必定會建立教會。這不單只是征服的證明，也是為了得到白神的加護，也就是說這是為了組成十字教的勢力圈的一種魔法儀式·裝置。

這種手法不單只是共和國在使用，在天穹大陸，不論那個國家都隨處可見，就算是潘多拉大陸，只要有人的勢力圈，就一定會建立某種神殿這種文化，這些都是從神學校的授業中學到的。

總而言之，莉莉在『神滅領域阿瓦隆』中，她口中的『樂園』中建造了祭拜妖精女王伊利斯的神殿，使自己能在範圍內使用加護。
但是，讓所有加護消失，被稱為神滅領域的場地效果是只存在於這裡非常特殊的東西。要讓其他加護無效化，作為聖地被數量眾多的人們所崇拜的強大信仰和巨大的龍穴，這兩個條件缺一不可。單憑一座神殿，是不可能有將其他加護無效化強大的影響力。

總而言之，只要在莉莉的樂園之內，菲奧娜就還能使用加護。當然，莉莉那邊能發揮的力量要更大一些，菲奧娜這邊要小一些，但絕不會對勝負不會造成多大的問題。
對轉移到這裡成功發動了『惡魔存在之證明』的菲奧娜來說，這樣一來，不安要素之一就消失，真是件讓人暗喜的事情。
「我覺得也差不多是時候了......」
在這裡能做的準備已經全部完成了，既然沒有對天空戰艦進行破壞工作的餘力和魔力，接下來就只能等著了。在甲板上裝備了連莉莉使用大砲也無法比擬的超巨大三聯裝砲的前端坐下，目不轉睛一動不動地凝視著結界的籠子，凝視著那白茫茫的白光面紗，老實的等著。
「喂，喂，睡著了嗎?—」
基本無視嫉妒雷伊。

除了像莉莉的惡靈偶爾會無意義的上來搭話，聽不到任何聲音。將自己關進的莉莉，應該是先飛去解決沙利葉和妮露，但是不論是爆音還是閃光,感覺不到任何東西。就好像只有這裡的時間停止了一般，安靜的讓人感到不適。

原來如此，這就是所謂的暴風雨前的平靜嗎。
深切地感受到這種感覺，終於，那一刻到來了。
「——讓你久等了，菲奧娜」
伴隨青白色的廣域結界消失，如同燃燒著一般全身纏繞著紅光的少女從天而降。
「不，也沒等那麼久，莉莉」
仿彿接下來會去一起購物一般輕鬆的回答。
將戴著頭上的三角帽擺正，整理了一下還不怎麼穿的習慣有些大的黑色斗篷的領口，菲奧娜從三聯裝主砲的砲口跳向甲板。
「「比原罪更加深重的罪惡」——『黑魔女·安迪米昂』·『惡魔存在之證明』」
第二次發動加護。
在甲板上輕盈著地的瞬間，菲奧娜的青髮長到腰部，頭上長出如水晶般透亮的美麗而又不祥的雙角。化身為擁有黃金惡魔之眼的魔人。
「你回來了，莉莉！」
「好像平安完成任務了呢，暫時安心了。謝謝你，雷伊」
啪嗒啪嗒在空中飛著的嫉妒，飛回了莉莉身邊。
「真的好像僅憑被限制的力量就打倒了沙利葉和妮露呢」
「但是差點就趕不上時間了。要不是兩人都有弱點就沒法打倒了，是強敵哦」
優雅的笑著回答的莉莉臉上看不到疲憊之色。也留有餘裕呢還是謙遜呢，不，事實上，不管是沙利葉還是妮露都應該給莉莉造成相當大的消耗。
只是，莉莉還有留有將其掩飾的餘裕。
「但是，菲奧娜，只有你就算是我不全力以赴就無法戰勝——來吧，雷伊，要上了哦」
「好的！加油。莉莉！」
嫉妒雷伊的身體完全化作真紅的光，像是被吸入莉莉的身體一樣消失了。

憑依完成，應該這麼說嗎。從莉莉全身，釋放出讓人不安的紅色薄霧一般的氣場，背後生長的蝶羽，不祥的光芒更加耀眼。
比起這些，從莉莉身上散發出的強烈的魔力波動讓菲奧娜的肌膚感到了嗶哩嗶哩的刺痛感。被嫉妒雷伊寄宿的莉莉，如今已經沒有時間限制了，

那份力量，與使徒之名相稱。不是被白神，被妖精之神所選中，愛之使徒。
終於，菲奧娜和認真起來的莉莉正面對峙起來。
「一直在想，這一刻總有一天會到來」
「嗯，我也是哦」
本應嫉妒而瘋狂的莉莉，但是，不管如何都不像菲奧娜記憶中的樣子，面帶優雅的笑容。
「菲奧娜，你是我最好的朋友，就是現在，也沒有改變」
「對我來說，莉莉是唯一的朋友哦」
不可思議的，心中十分平靜。
沒有任何迷茫。所以，沒有任何交涉和說服。也沒有這個必要。
「這就是，最後的戰鬥了。撒，上吧菲奧娜。你的全部，由我接下」
「好，莉莉，要上了哦」
要說為什麼，因為這就是命運。這是從愛上名為黑乃的男子之時就已經決定，無法改變的宿命。
「墜落吧，原初殘留的火焰『煉獄結界』」
「『妖精結界』，全開」
妖精莉莉和魔女菲奧娜。為一個男人而賭上最純粹的愛的巔峰之戰，開始了。
